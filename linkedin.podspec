Pod::Spec.new do |s|
  s.name             = 'linkedin'
  s.version         = '0.0.7'
  s.license         =  { :type => 'MIT' }
  s.homepage         = 'https://developer.linkedin.com/docs/ios-sdk'
  s.authors         = { 'LinkedIn' => '-' }
  s.summary         = 'A cocoa pod containing the LinkedIn framework.'
  s.source           = { :git => 'https://gitlab.com/seriyvolk83/linkedinsdk.git' }
  s.vendored_frameworks = 'linkedin.framework'
  s.source_files = 'linkedin.framework/Headers/*.h'
  
  s.platform     = :ios
  s.ios.deployment_target = '10.0'
  s.requires_arc = true
end
